set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"python autocomplete
Plugin 'davidhalter/jedi-vim'

"ruby autocomplete
Bundle 'vim-ruby/vim-ruby'

"auto-closing html/xml tags
Plugin 'alvan/vim-closetag'

Plugin 'ervandew/supertab'

" " All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line

set number
syntax on

set backspace=2
set tabstop=4 shiftwidth=4 softtabstop=4
set expandtab

set ruler
colorscheme euphrasia
set visualbell

set hlsearch

nn <CR> :nohlsearch<CR>/<BS><CR>

set background=dark

:autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/
:autocmd BufWinLeave * call clearmatches()
:highlight ExtraWhitespace ctermbg=darkgreen

set cursorline

set scrolloff=6

let g:SuperTabDefaultCompletionType = "<c-x><c-o>"

au GUIEnter * simalt ~x