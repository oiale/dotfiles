# .bashrc
# for 4.4 + only

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# source the script from git to use __git_ps1
source ~/.bash_git

# User specific aliases and functions

# Color Codes

# Reset:
Reset=`tput sgr0`

# Regular colors:
Black=`tput setaf 0`
Red=`tput setaf 9`
Green=`tput setaf 46`
Yellow=`tput setaf 11`
Blue=`tput setaf 45`
Purple=`tput setaf 135`
Cyan=`tput setaf 35`
White=`tput setaf 255`

bold=`tput smso`
offbold=`tput rmso`

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

alias ls='ls --color=auto'
alias ll='ls -la --color=auto'

export PROMPT_DIRTRIM=3

export PS1="\`
    printf '\n(\[$Purple\]\w\[$Reset\])\[$Yellow\]'
    __git_ps1
    case \$? in
        0)
            printf '\[$Reset\]\n\[$Green\] >\[$Reset\] '
            ;;
        *)
            printf '\[$Reset\]\n\[$Red\] >\[$Reset\] '
            ;;
    esac
\`"

export VISUAL="vim"

gcompush() {
    git commit -m "${1}" && git push
}

gaacompush () {
    git add . && git commit -m "${1}" && git push
}